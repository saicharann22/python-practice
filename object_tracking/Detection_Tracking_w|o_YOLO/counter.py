"""Tracking the vehicles and
    counting the no of vehicles passed
"""

from cv2 import cv2
#from cv2 import GaussianBlur
import numpy as np

cap = cv2.VideoCapture("video.mp4") #accessing the video

min_Width_Rect = 80 #min_width_rectangle
min_Height_Rect = 80 #min_width_rectangle

count_line_position = 550
#initialize substractor usign Guassian model
algo = cv2.bgsegm.createBackgroundSubtractorMOG()


def center_handle(x,y,w,h):
    """center position of the object/vehicle
    """
    x1 = int(w/2)
    y1 = int(h/2)
    cx = x + x1
    cy = y + y1
    return cx,cy

detect = []
offset = 6 #Allowable error between pixels
counter = 0

while True:
    ret,frame1 = cap.read()
    grey = cv2.cvtColor(frame1,cv2.COLOR_BGR2GRAY) #greyimage conversion
    blur = cv2.GaussianBlur(grey,(3,3),5) #image smoothening
    # applying  on each frame
    img_sub  = algo.apply(blur)
    #dilation expands white region of foreground pixels
    #erosion shrinks
    dilat = cv2.dilate(img_sub,np.ones((5,5)))
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))
    #Morph close involves dilation followed by erosion to remove  noise
    dilatada = cv2.morphologyEx(dilat,cv2.MORPH_CLOSE,kernel)
    dilatada = cv2.morphologyEx(dilatada,cv2.MORPH_CLOSE,kernel)
    counterShape,h = cv2.findContours(dilatada,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

    #count line
    cv2.line(frame1,(25,count_line_position),(1200,count_line_position),(255,127,0),3)

    for (i,c) in enumerate(counterShape):
        (x,y,w,h) = cv2.boundingRect(c)
        validate_counter = (w >= min_Width_Rect) and (h>= min_Height_Rect)
        if not validate_counter:
            continue
        cv2.rectangle(frame1,(x,y),(x+w,y+h),(0,255,0),2)
        #cv2.putText(frame1,"vehicle:"+ str(counter),
        #            (x,y-20),cv2.FONT_HERSHEY_SIMPLEX,1,(255,244,0),2)

        center = center_handle(x,y,w,h)
        detect.append(center)
        cv2.circle(frame1,center,4,(0,0,255),-1)

        for (x,y) in detect:
            if y < (count_line_position + offset) and y > (count_line_position - offset):
                counter = counter + 1
            cv2.line(frame1,(25,count_line_position),(1200,count_line_position),(0,127,25),3)
            detect.remove((x,y))
            print("Vehicle counter:" + str(counter))

    #change the line color once the vehicle passes the line
    cv2.putText(frame1,"VEHICLE COUNTER :"+ str(counter),
                (450,70),cv2.FONT_HERSHEY_SIMPLEX,2,(0,0,255),5)

    #cv2.imshow('Detector',dilatada)

    cv2.imshow('video_original',frame1)
    key = cv2.waitKey(30)
    if key == 27:
        break
cv2.destroyAllWindows()
cap.release()
