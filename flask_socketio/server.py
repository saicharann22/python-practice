from flask import Flask, render_template
from flask_socketio import SocketIO, emit

app = Flask(__name__,template_folder='html_template')
#app.config['SECRET_KEY'] = 'secret!' #
socketio = SocketIO(app)

@app.route('/')
def index():
    return render_template('index.html')

@socketio.event
def chat_message(message):
    print("--------------",message)
    emit('chat message', message,broadcast=True)

if __name__ == '__main__':
    socketio.run(app)
