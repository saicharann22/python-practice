
import re
from idna import valid_label_length
from itsdangerous import json
import redis
from flask import Flask, render_template, request

#flask app
app = Flask(__name__,template_folder='redis_html')

# connecting to redis database
redis_cache_memory = redis.Redis(host='localhost',
                                 port=6379,
                                 db=0,charset = "utf-8",
                		  decode_responses=True)

@app.route("/")   #routing
def home():        #directing to home html
    return render_template("redis_home.html")

@app.route('/set',methods = ["GET","POST","PUT","DELETE"])
def set():
    key = request.json.get('key')
    value = request.json.get('value')
    #print(request.json.get)
    print(value)

    if redis_cache_memory.exists(key):
        return "key already exists"
    else:
        redis_cache_memory.set(key, value)
        print("success")
        #return {"HELD":"HOLD"}
        return "values has been set"

@app.route('/get',methods = ["GET","POST","PUT","DELETE"])
def get():
    key = request.json.get('key')
    print(redis_cache_memory.get(key))
    if redis_cache_memory.exists(key):
        a = redis_cache_memory.get(key)
        return a
    else:
        return f"{key} -- key does not exists"

@app.route('/add_to_list',methods = ["GET","POST","PUT","DELETE"])
def add_to_list():
    l1 = request.json.get('l1')
    a = request.json.get('a')
    b = request.json.get('b')
    redis_cache_memory.lpush(l1,a,b)
    return "added to list"

@app.route('/get_from_list',methods = ["GET","POST","PUT","DELETE"])
def get_from_list():
    l2 = request.json.get('l2')
    if redis_cache_memory.exists(l2):
        a  = redis_cache_memory.lrange(l2,start=0,end=-1)
        print(type(a))
        return str(a)
        #return "ok"
    else:
        return "list does not exist"


if __name__ == "__main__":
    app.run(debug= True)
