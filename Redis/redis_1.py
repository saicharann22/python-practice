from typing import Mapping
import redis

r = redis.Redis(host = "localhost",port = 6379,db = 0)
r.set("name","charan")
r.set("region","ap")

name = r.get("name")
region = r.get("region")
print(name)
print(region)

#creating and push values into list
r.lpush("dabba_1" ,"redis" ,"a","b")
list_a = r.lrange("dabba_1", start=0, end=3)
print(list_a)

#adding into sets
r.sadd("set_a" ,"v1","v2")
r.sadd("set_a","v3")
s = r.smembers("set_a")
print(s)


a = r.get("name")
print(a)


#size of database
dbsize = r.dbsize()
print(dbsize)

#setting multiple keys and values together
dict_a = {"stirng1":"ram","string2":"krishna"}
for k,v in dict_a.items():
    pair = {k:v}
    r.mset(pair)

print(r.get("string2"))

#using hashes(key,field,value)
r.hset(1,"wind","25atm")
hg = r.hget(1,"wind")
print(hg)
