# Title
Facial Key points Detection

# Description
The  program  is used for the detection
of the key features of the face in the given 
image like Eyes,Nose,Lips etc


# Requirements 
- Open cv

- dlib(consists of ML algorithms)

- shape_predictor_68_face_landmarks.dat file


# Work flow 

- The image which is used for the facial key point 
- Detections is converted to "Grey scale image"
  using "CV2.COLOR_BGR2GRAY()" method

- Create a Detector object from dlib library
  which detects faces in the image by using "get_frontal_face_detector" method
  as shown below

![App Screenshot](https://bitbucket.org/saicharann22/python-practice/raw/7fb702c48677cca34db5ced3156bde3a33efeea7/facial_key_points_detection/output/Faces_detection.png)

- Later Create a "Predictor Object" Which predicts the "Shape" 
  of the face 
- By sending the grey scale image to the predictor objecct 
  "key features" of the subject like Eyes,Nose etc are Detected
  as shown below

# Outputs

![App Screenshot](https://bitbucket.org/saicharann22/python-practice/raw/7fb702c48677cca34db5ced3156bde3a33efeea7/facial_key_points_detection/output/detection1.png)

![App Screenshot](https://bitbucket.org/saicharann22/python-practice/raw/7fb702c48677cca34db5ced3156bde3a33efeea7/facial_key_points_detection/output/detection2.png)




