import cv2
import dlib #library contains Ml algo

img = cv2.imread("face2.jpg")
img = cv2.resize(img,(0,0),None,0.1,0.1)
img_original = img.copy()

#object to find faces in the pics
detector = dlib.get_frontal_face_detector()
#object to detect the keyparts/shapes in the face
predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

imgGray  = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
faces =    detector(imgGray) #detects all faces in the image

for face in faces:
    x1,y1 = face.left(),face.top()
    x2,y2 = face.right(),face.bottom()
    img_original = cv2.rectangle(img,(x1,y1),(x2,y2),(0,255,0),2) #face bound box
    landmarks = predictor(imgGray,face) #finds all landmarks in the face
    for n in range(68):
        x = landmarks.part(n).x
        y = landmarks.part(n).y
        # (img,center_point,radius,color)
        cv2.circle(img_original,(x,y),5,(50,50,255),cv2.FILLED)

cv2.imshow("Original",img_original)
cv2.waitKey(0)