import cv2
cap = cv2.VideoCapture("highway.mp4")

# Object detection from video
object_detector = cv2.createBackgroundSubtractorMOG2(history=100,
                                                     varThreshold=40)
while True:
    retvals, frame = cap.read()
    height, width, _ = frame.shape

    roi = frame[340: 720,500: 800] # Region of interest

    # Object Detection
    mask = object_detector.apply(roi)
    _, mask = cv2.threshold(mask, 254, 255, cv2.THRESH_BINARY)
    contours, _ = cv2.findContours(mask,
                                   cv2.RETR_TREE, #returns the contours
                                   cv2.CHAIN_APPROX_SIMPLE) #endpoints
    for cnt in contours:
        # Calculating area and remove small elements
        # and tracking the objects
        area = cv2.contourArea(cnt)
        if area > 100:
            x, y, w, h = cv2.boundingRect(cnt)
            cv2.rectangle(roi, (x, y), (x + w, y + h), (0, 255, 0), 3)
            print([x,y,w,h])

    cv2.imshow("roi", roi)
    cv2.imshow("Frame", frame)
    cv2.imshow("Mask", mask)

    key = cv2.waitKey(30)
    if key == 27:  #esc key
        break

cap.release()
cv2.destroyAllWindows()

