import cv2
import pytesseract
import numpy as np

image = cv2.imread('/home/neosoft/flask_folder/OCR/nature.jpg')
cv2.imshow('original_img',image)
cv2.waitKey(0)
#linear_interpolation
image_scaled1 = cv2.resize(image,None,fx=0.5,fy= 0.5)
cv2.imshow('resized_image--linear Interpolated',image_scaled1)
cv2.waitKey(0)
cv2.destroyAllWindows()

#inter cubic
image = cv2.imread('/home/neosoft/flask_folder/OCR/nature.jpg')
cv2.imshow('original_img',image)
cv2.waitKey(0)

image_scaled2 = cv2.resize(image,None,
                           fx=2,
                           fy=2,
                           interpolation = cv2.INTER_CUBIC)
cv2.imshow('resized_image--cubic Interpolated',image_scaled2)
cv2.waitKey(0)
cv2.destroyAllWindows()

#skewed size interpolation/area
image = cv2.imread('/home/neosoft/flask_folder/OCR/nature.jpg')
cv2.imshow('original_img',image)
cv2.waitKey(0)

image_scaled2 = cv2.resize(image,(400,400),
                           interpolation = cv2.INTER_AREA)
cv2.imshow('resized_image--cubic Interpolated',image_scaled2)
cv2.waitKey(0)
cv2.destroyAllWindows()#end


