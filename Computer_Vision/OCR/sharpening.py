import cv2
import numpy as np

image = cv2.imread('/home/neosoft/flask_folder/OCR/nature.jpg')
sharpening_kernal = np.array([[-1,-1,-1],
                              [-1,9,-1],
                              [-1,-1,-1]])

cv2.imshow('original_img',image)
cv2.waitKey(0)

sharp_image = cv2.filter2D(image,-1,sharpening_kernal)
cv2.imshow('sharpened_image',sharp_image)
cv2.waitKey(0)
cv2.destroyAllWindows()#end
