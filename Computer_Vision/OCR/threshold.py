import cv2
import numpy as np

image = cv2.imread('/home/neosoft/flask_folder/OCR/img2.png')

a1,threshold_image1 = cv2.threshold(image,   #greyscale image
                              125,
                              255,
                              cv2.THRESH_BINARY)
a2,threshold_image2 = cv2.threshold(image,125,255,cv2.THRESH_BINARY_INV)
a3,threshold_image3 = cv2.threshold(image,127,255,cv2.THRESH_TRUNC) #truncated @ 127
a4,threshold_image4 = cv2.threshold(image,127,255,cv2.THRESH_TOZERO)
a5,threshold_image5 = cv2.threshold(image,127,255,cv2.THRESH_TOZERO_INV)


cv2.imshow("Threshold_bianry",threshold_image1)
cv2.waitKey()

cv2.imshow("Threshold_binary_inverse",threshold_image2)
cv2.waitKey()

cv2.imshow("Threshold_trunc",threshold_image3)
cv2.waitKey()

cv2.imshow("Threshold_to_zero",threshold_image4)
cv2.waitKey()

cv2.imshow("Threshold_to_zero_inv",threshold_image5)
cv2.waitKey()
cv2.destroyAllWindows()#end
