import cv2
import pytesseract
import numpy as np

image1 = np.zeros((256,512,3),np.uint8)#color image
#zero represents black color
image_bw = np.zeros((256,512),np.uint8)#black & white image

#plain dark images
cv2.imshow("black_rectangle(color)",image1)
cv2.waitKey()
cv2.destroyAllWindows()

cv2.imshow("black_rectangle(b&W)",image_bw)
cv2.waitKey()
cv2.destroyAllWindows()

#drawing a lines
cv2.line(image1 , (0,0), (511,251), (255, 42, 165), 2)
cv2.imshow("line",image1)
cv2.waitKey()
cv2.destroyAllWindows()

#drawing a rectangle inside
cv2.rectangle(image1 , (150,50), (350,150), (255, 42, 165), -1)
cv2.imshow("rectangle",image1)
cv2.waitKey()
cv2.destroyAllWindows()

#drawing circle
cv2.circle(image1 , (150,150), 80, (0, 255, 255), 2)
cv2.imshow("circle & rectangle & line",image1)
cv2.waitKey()
cv2.destroyAllWindows()

#writing a text
text_image = np.zeros((256,512,3),np.uint8)#color image
cv2.putText(text_image ,
           "hello world",
           (50,50), #starting point
           cv2.FONT_HERSHEY_COMPLEX,
           2,  #fontsize
           (0, 255, 255), #color
           2)   #thickness
cv2.imshow("text",text_image)
cv2.waitKey()
cv2.destroyAllWindows() #end



