from asyncio import start_server
from turtle import heading, st, width
import cv2
import pytesseract
import numpy as np

image = cv2.imread('/home/neosoft/flask_folder/OCR/nature.jpg')

height,width = image.shape[:2]

start_row ,start_col = int(height*0.55),int(width*0.25)
end_row ,end_col = int(height*0.75) ,int(width*0.75)

cropped = image[start_row:end_row,start_col:end_col]
cv2.imshow('original_img',image)
cv2.waitKey(0)

cv2.imshow('cropped_image',cropped)
cv2.waitKey(0)
cv2.destroyAllWindows()#end
