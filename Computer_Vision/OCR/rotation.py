import cv2
import pytesseract
import numpy as np

image = cv2.imread('/home/neosoft/flask_folder/OCR/nature.jpg')
cv2.imshow('original_img',image)
cv2.waitKey(1000)

height,width = image.shape[:2]

rotation_matrix = cv2.getRotationMatrix2D((width/2,height/2), #startpoint
                                          45 , #angle
                                          0.5)  #scale
rotation_image = cv2.warpAffine(image, rotation_matrix, (width,height))
cv2.imshow('rotated_img',rotation_image)
cv2.waitKey(1000)
cv2.destroyAllWindows()

#transpose method
cv2.imshow('original_img',image)
cv2.waitKey(1000)
cv2.destroyAllWindows()

transposed_image = cv2.transpose(image)
cv2.imshow("transposed_image",transposed_image)
cv2.waitKey(1000)
cv2.destroyAllWindows()

#flipped
cv2.imshow('original_img',image)
cv2.waitKey()
#cv2.destroyAllWindows()

flipped_image = cv2.flip(image,1)#horizontal flip(1)
                                 #vertical flip(-1)
cv2.imshow("transposed_image",flipped_image)
cv2.waitKey()
cv2.destroyAllWindows()#end

