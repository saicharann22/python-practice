import cv2
from numpy import imag
import pytesseract

image = cv2.imread('/home/neosoft/flask_folder/OCR/nature.jpg')
cv2.imshow('original_img',image)
cv2.waitKey(1000)
cv2.destroyAllWindows()

#one method
grey_image1 = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
cv2.imshow('gray_scale_img',grey_image1)
cv2.waitKey(1000)
cv2.destroyAllWindows()

'''
#second method
grey_image2 = cv2.imread('/home/neosoft/flask_folder/OCR/nature.jpg',0)

cv2.imshow('gray_image2',grey_image2)
cv2.waitKey(1000)
cv2.destroyAllWindows()

''' #end
