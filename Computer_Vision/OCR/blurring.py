import cv2
import numpy as np
#blur
image = cv2.imread('/home/neosoft/flask_folder/OCR/nature.jpg')
cv2.imshow('original_img',image)
cv2.waitKey(0)
kernal_b = np.ones((7,7),np.float32)/49 #box blur kernal
blurred_image =  cv2.filter2D(image,
                              -1,      #o/p with same depth
                              kernal_b)
cv2.imshow("blurred_image",blurred_image)
cv2.waitKey()
cv2.destroyAllWindows()

#Blur
cv2.imshow('original_img',image)
cv2.waitKey(0)
img1 = cv2.blur(image,(3,3))          #kernal matrix
cv2.imshow("Blurred",img1)
cv2.waitKey()

#gaussain blur
img2 = cv2.GaussianBlur(image,
                        (7,7),        #kernal matrix
                        0)            #o/p image size
cv2.imshow("Gaussian Blur",img2)
cv2.waitKey()

#median blur
img3 = cv2.medianBlur(image,5)        #kernal size
cv2.imshow("Median Blur",img3)
cv2.waitKey()

#bialteral blur
img4 = cv2.bilateralFilter(image,
                           9,        #diameter of pixel
                           75        #sigmacolor
                           ,75)      #sigmaspace
cv2.imshow("bilateral Blur",img4)
cv2.waitKey()



cv2.destroyAllWindows() 

