from turtle import heading, width
import cv2
import pytesseract
import numpy as np

image = cv2.imread('/home/neosoft/flask_folder/OCR/nature.jpg')
cv2.imshow('original_img',image)
cv2.waitKey(1000)

height,width = image.shape[:2]
new_h,new_w = height/4, width/4

T = np.float32([[1,0,new_w],
                [0,1,new_h]])
image_translation = cv2.warpAffine(image,T,(width,height))
cv2.imshow('translated_img',image_translation)
cv2.waitKey(1000)
cv2.destroyAllWindows()#end
