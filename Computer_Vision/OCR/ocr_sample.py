import cv2
import pytesseract

# read image
img = cv2.imread('/home/neosoft/flask_folder/OCR/practice_image.png')
img = cv2.resize(img,(400,450))
cv2.imshow("Image",img)

cv2.waitKey(2000)

cv2.destroyAllWindows()

# configurations
CONFIG = ('-l eng --oem 1 --psm 3')
# pytessercat
text = pytesseract.image_to_string(img, config=CONFIG)
# print text
text = text.split('\n')
print(text) 
