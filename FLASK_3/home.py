import sqlite3
from flask import Flask,render_template,request
from flask import  jsonify
#import json
#from crypt import methods
#from distutils.log import debug
#import re
#from tkinter.messagebox import RETRY
#from flask import *
#from pkg_resources import parse_requirements


app = Flask(__name__)

def db_connection():
    conn = None
    try:
        conn = sqlite3.connect("adding_2.sqlite")
    except sqlite3.error as e:
        print(e)
    return conn
app = Flask(__name__,template_folder='Home_html')

@app.route("/")   #routing
def home():
    return render_template("ajaxhome.html")   #directing to home html


@app.route("/endapi",methods = ["GET","POST","PUT","DELETE"])
def endapi():
    conn = db_connection()   #getting a database connection
    cursor = conn.cursor() #using instance

    if request.method == "DELETE":
        #id = request.json.get('id')
        sql = """ DELETE  FROM addition_2
        """
        cursor = cursor.execute(sql)
        conn.commit()
        return "ROW DELETED"

    if request.method == "GET":
        sql = """ SELECT * FROM addition_2 """
        cursor = cursor.execute(sql)
        conn.commit()
        return jsonify(cursor.fetchall())


    if request.method == "POST":
        a = int(request.form.get('a')) #getting values from form
        b = int(request.form.get('b'))
        total = a + b

        #query
        print(a)

        sql = """INSERT INTO addition_2(a,b,total)
                 VALUES (?,?,?)
              """
        cursor = cursor.execute(sql, (a,b,total))  #executing the query
        conn.commit()

        return render_template('endapi.html',a = a,b = b)

    if request.method == "PUT":
        a  = int(request.json.get('a'))
        b = int(request.json.get('b'))
        id = int(request.json.get('id'))
        total = a + b
        sql = """UPDATE  addition_2
                 SET  a = ?,
                      b = ?,
                      total = ?
                WHERE id = ?
         """
        cursor = cursor.execute(sql, (a,b,total,id))  #executing the query
        conn.commit()
        return "values updated"

@app.route("/endapi/<int:id>", methods=["GET", "PUT", "DELETE"])
def single_operation(id):
    conn = db_connection()
    cursor = conn.cursor()

    each = None
    if request.method == "GET":
        cursor.execute("SELECT * FROM addition_2 WHERE id=?", (id,))
        rows = cursor.fetchall()
        for r in rows:
            each = r
        if each is not None:
            return jsonify(each), 200
        else:
            return "Something wrong", 404

    if request.method == "PUT":
        a  = int(request.json.get('a'))
        b = int(request.json.get('b'))
        c = a + b
        sql = """UPDATE addition_2
                SET a=?,
                    b=?,
                    total=?
                WHERE id=? """

        #a = request.form["a"]
        #b = request.form["b"]

        updated_book = {
            "id": id,
            "a": a,
            "b": b,
            "total": c,
        }
        conn.execute(sql, (a, b, c, id))
        conn.commit()
        return jsonify(updated_book)

    if request.method == "DELETE":
        sql = """ DELETE FROM addition_2 WHERE id=? """
        conn.execute(sql, (id,))
        conn.commit()
        return "The row with id: {} has been deleted.".format(id), 200


if __name__ == '__main__':
    app.run(debug = True)

