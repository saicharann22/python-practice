import sqlite3

conn = sqlite3.connect("adding_2.sqlite")  #connection to the local database
                        #if database not existed new one gets created
                        
cursor = conn.cursor()  #current database connection instance

#CREATING  A TABLE
sql_query = """ CREATE TABLE addition_2 (  
    id integer PRIMARY KEY,  
    a Int NOT NULL,
    b Int NOT NULL,
    total int NOT NULL
)"""
cursor.execute(sql_query) #executed the query using cursor 


