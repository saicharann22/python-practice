#Importing the libraries
#import string
from nltk.tokenize import sent_tokenize
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from nltk.stem.wordnet import WordNetLemmatizer
#import sklearn
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer

import nltk
nltk.download
nltk.download('punkt')  #breaks a sentence into list of sentences
nltk.download('stopwords') #dict of stopwords
nltk.download('wordnet')   #database of nouns,adj,etc...to find relation btwn words


#Creating bunch of sentences
raw_docs = ["Hello good evening , make a good use of the resources",
"Just writing it for the demo PURPOSE to make me understand the basics",
"The point is to know HOW it works on simple  data."]


raw_docs = [each.lower() for each in raw_docs]  #list comprehension to convert to lowercase

for each in raw_docs:
    print(each)

##Tokenization

##Sentence tokenization


sent_token = [sent_tokenize(doc) for doc in raw_docs]

for each in sent_token:
    print(each)

## Word tokenization


tokenized_docs = [word_tokenize(doc) for doc in raw_docs]

for each in tokenized_docs:
    print(each)

##Removin stop words



tokenized_docs_no_stopwords = []

for doc in tokenized_docs:
    new_term_vector = []
    for word in doc:
        if not word in stopwords.words('english'):  #list of all stopwords
            new_term_vector.append(word)

    tokenized_docs_no_stopwords.append(new_term_vector)

print(tokenized_docs_no_stopwords)

##Pos Tagging

import spacy  #importing llibrary
nlp=spacy.load('en_core_web_sm')  #loading "en_core_web_sm" module
text='It took me more than two hours to translate a few pages of English'

for token in nlp(text):
    print(token.text, '=>',token.pos_,'=>',token.tag_)

#print(token.text,'=>',token.dep_,'=>',token.head.text)

##Stemming and Lemmatization

##Stemming



porter = PorterStemmer()
preprocessed_docs_1 = []

for doc in tokenized_docs_no_stopwords:
    final_doc = []
    for word in doc:
        final_doc.append(porter.stem(word))

    preprocessed_docs_1.append(final_doc)

print(preprocessed_docs_1)

##Lemmatizing



wordnet = WordNetLemmatizer()

preprocessed_docs_2 = []

for doc in tokenized_docs_no_stopwords:
    final_doc = []
    for word in doc:
        final_doc.append(wordnet.lemmatize(word)) #use wordvocablary(wordnet) for  root form

    preprocessed_docs_2.append(final_doc)

print(preprocessed_docs_2)

#Count Vectorizer


text = ["hello, my name is raghu and I am a working professional."] #doc1
text1 = ["You have to keep expanding your horigins for a professional growth, raghu"] #doc2

vectorizer = CountVectorizer() #object creation

vectorizer.fit(text)

print(vectorizer.vocabulary_) #document vector

newvector = vectorizer.transform(text1)   #fitting the doc into the vector

print(newvector.toarray())

#Tf-Idf



text = ["Raghu is a data scientist in India",
	"There is a demand in data science",
		"Data Science is a promising career"]

# create the transform
vectorizer = TfidfVectorizer()

# tokenize and build vocab
vectorizer.fit(text)

# summarize
print(vectorizer.vocabulary_)

#Focus on IDF VALUES
print(vectorizer.idf_)

text_as_input = text[2]
#text_as_input

# encode document
vector = vectorizer.transform([text_as_input])  #fitting text into the vectorizer

# summarize encoded vector
print(vector.toarray())

