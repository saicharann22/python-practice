from gc import get_stats
from pathlib import Path
import re
from xml.dom.xmlbuilder import DocumentLS
import flask
from flask import Flask, render_template, request
from numpy import broadcast
import redis
from streamer import *
import atexit
import asyncio

from flask import Flask, render_template
from flask_socketio import SocketIO, emit

from common import cache

global get_status
get_status = False

app = Flask(__name__,static_url_path='',template_folder = "flask_obj_detect.html")
cache.init_app(app=app, config={"CACHE_TYPE": "filesystem",'CACHE_DIR': Path('/tmp')})


socketio = SocketIO(app)

# connecting to redis database
redis_cache_memory = redis.Redis(host='localhost',
                                 port=6379,
                                 db=2,
                                 charset = "utf-8",
                                 decode_responses=True)

@app.route("/")
def home():
    return render_template("index.html")

@app.route('/device-uuid.js')
def static_file():
    #print("unique code for device")
    return app.send_static_file("device-uuid.js")

@app.route('/on_off',methods = ["GET","POST"])
def on_off():
    print("value")
    return "heloo"
    # value = request.json.get("onstatus")
    # uuid = request.json.get("uuid")
    # print(value)
    # #print(uuid)

    # redis_cache_memory.set(str(uuid),str(value))
    # redis_cache_memory.set("status",str(value))

    # if value == True:
    #     yourThread = threading.Thread()
    #     yourThread.start()
    #     yourThread.run()
    #     streamer(True)
    #     # return "ON"
    # elif value == False:
    #     # yourThread.cancel()
    #     # atexit.register(interrupt)
    #     # streamer(False)
    #     print('stop babe')
    #     # return "OFF"

@socketio.event
def frames(message):
    print("--------------",message)
    emit('chat message', message,broadcast=True) #sends to all clients

#global get_status
get_status = False

@socketio.event
def set_status(value):
    get_status = value
    print("")

@socketio.event
def off(value):
    print('socket offfff')
    cache.set("camera_on_status", False)

@socketio.event
def on_off(raw_obj):
    print("socket recieved...")
    redis_cache_memory.set(str(raw_obj["uuid"]),str(raw_obj["onstatus"]))
    cache.set("camera_on_status", True)

    streamer(raw_obj["onstatus"])
    #emit("response",raw_obj["onstatus"],broadcast = True) #send to clients
    #loop = asyncio.get_event_loop()
    #f1 = loop.create_task(streamer(raw_obj["onstatus"]))
    #await asyncio.wait(f1)
    print("done babe")


if __name__ == '__main__':
    #app.run(debug= True)
    socketio.run(app)  # http://127.0.0.1:5000

