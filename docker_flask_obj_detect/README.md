
# Project Title
Object Detection from Flask application

# Description
Here in this application we will be using a webcam 
to get the live video visuals/frames and detect the objects
present in those frames 
and display the end results dynamically on the
web browser using web sockets

# Requirements 
- Open cv

- flask

- flask_socketio

- Yolo v3

- coco Names(80 classes)

- Numpy

- Redis

# Work flow 

When the application is runned (main.py)

![App Screenshot](https://bitbucket.org/saicharann22/python-practice/raw/56736271b134e543401b736bec0f612d571688ac/docker_flask_obj_detect/outputs/home_page.png)

Toggle button will be display to access the webcam

Based on the web access/camera status the video frames will be obtained
and sent to the streamer(streamer.py file)

which  reads  the obtained frames to the resizes,encodes into  base64 format
and send the frames for detection using YOLO ,COCO classes
present in the Viewer file(viewer.py)

The detected frames are sent/emitted to the server using websockets
which are received by the client(index.html) 
and displayed in the browser as shown below

# Outputs

![App Screenshot](https://bitbucket.org/saicharann22/python-practice/raw/d759dd42738cad40893035ac3d7e0f5b9464d2cf/docker_flask_obj_detect/outputs/Screenshot%20from%202022-05-19%2019-55-51.png)

![App Screenshot](https://bitbucket.org/saicharann22/python-practice/raw/d759dd42738cad40893035ac3d7e0f5b9464d2cf/docker_flask_obj_detect/outputs/Screenshot%20from%202022-05-19%2019-58-06.png)

![App Screenshot](https://bitbucket.org/saicharann22/python-practice/raw/d759dd42738cad40893035ac3d7e0f5b9464d2cf/docker_flask_obj_detect/outputs/Screenshot%20from%202022-05-19%2020-02-24.png)

![App Screenshot](https://bitbucket.org/saicharann22/python-practice/raw/d759dd42738cad40893035ac3d7e0f5b9464d2cf/docker_flask_obj_detect/outputs/Screenshot%20from%202022-05-19%2020-03-25.png)



