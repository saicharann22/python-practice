"""flask_object detection
   flask_object detection
   where the camera will be
   accessed from the web browser
   and detections made will
   be displayed in the browser
   dynamically by using toggle button
   """
#from threading import Timer
#import flask
#from numpy import broadcast
#from viewer import viewer
from flask import Flask, render_template
from flask_socketio import SocketIO
from streamer import image_conversion

app = Flask(__name__,static_url_path='',template_folder = "flask_obj_detect.html")
socketio = SocketIO(app)

# connecting to redis database
# redis_cache_memory = redis.Redis(host='localhost',
#                                  port=6379,
#                                  db=2,
#                                  charset = "utf-8",
#                                  decode_responses=True)

@app.route("/")
def home():
    """Directed to Home page where the
    user can have the access for toggle button
    """
    return render_template("index.html")

@app.route('/device-uuid.js')
def static_file():
    """Can get unique id of the device(uuid)
    """
    #print("unique code for device")
    return app.send_static_file("device-uuid.js")

@socketio.event
def img64(val):
    """Receives the Image after detection
    and sent to the browser
    for displaying in the browser
    """
    print(val)
    print("got img...")

# @socketio.event
# def on(value):
#     """Toggle off"""
#     print('toggle offfff')
#     print(value)
#     streamer(True)

@socketio.event
def on_off_socket(value): #
    """When Toggle is on
       the frame is received and
       converted into byte format
       and sent for detection in the viewer
    """
    print(value)
    image_conversion()

if __name__ == '__main__':
    #app.run(debug= True)
    socketio.run(app,host="0.0.0.0")  # http://127.0.0.1:5000
