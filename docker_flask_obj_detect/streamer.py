"""Streamer file
   mainly used for streaming
   from webcam, converting the
   frames to byte format
   for the detection to occur
   in the frames
"""
import base64
import random
import os
from cv2 import cv2
import zmq
from viewer import viewer
#from random import random
#import time
#import redis
#import threading
#from flask_socketio import SocketIO
#import socket

#global camera
#current_frame = 0

def image_conversion():
    """ The frames obtained will be
        converted to base64 and
        sent for detection
    """
    camera = cv2.VideoCapture(0)
    ret, frame = camera.read()
    frame = cv2.resize(frame, (800, 480))  # resize the frame
    _, buffer = cv2.imencode('.jpg', frame)
    jpg_as_text = base64.b64encode(buffer)
    # footage_socket.send(jpg_as_text)

    # saving the frames into  folder
    try:
        if not os.path.exists('frames_folder'):
            os.makedirs('frames_folder')
    except OSError:
        print('Error')

    if ret:
        name = './frames_folder/frame' + str(random.random()) + '.jpg'
        #print('captured...' + name)
        cv2.imwrite(name, frame)
        frame = cv2.resize(frame, (800, 480))  # resize the frame
        _, buffer = cv2.imencode('.jpg', frame)
        jpg_as_text = base64.b64encode(buffer)
        viewer(jpg_as_text)
        #current_frame += 1


def streamer(value):
    """streamer function"""
    print(value, 'streamer - 16')
    #a = main.get_status

    if value:
        print("done")
        context = zmq.Context()
        footage_socket = context.socket(zmq.PUB)
        footage_socket.connect('tcp://localhost:5555')

    else:
        print("Not done")
        #camera = cv2.VideoCapture(0)
        #camera.release()
        cv2.destroyAllWindows()
