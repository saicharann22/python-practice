"""stock_client"""

from __future__ import absolute_import
import sys
import zmq

context = zmq.Context()
socket = context.socket(zmq.SUB)

print("Collecting updates from stock server...")
socket.connect ("tcp://localhost:5556")

product_filter = sys.argv[1:] if len(sys.argv) > 1 else ["AAPLE"]

for each in product_filter:
    socket.setsockopt_unicode(zmq.SUBSCRIBE, each)

while True:
    string = socket.recv_unicode()
    stock, price = string.split()

    print("{}: {}".format(stock, price))
