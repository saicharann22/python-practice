#   Publishes random stock updates
"""stock_server"""
from __future__ import absolute_import
import time
import random
import zmq

context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind("tcp://*:5556") #   Binds PUB socket to tcp://*:5556
list_a = ['AAPLE', 'GOOGLE', 'MCRSFT', 'AMZN']

while True:
    product = random.choice(list_a)
    price = random.randrange(20,700)

    MSG = "{}: {}".format(product, price)
    print(MSG)
    socket.send_unicode(MSG)
    time.sleep(0.5)
