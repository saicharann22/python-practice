import sys
import time
import zmq

context = zmq.Context()

#  Socket to talk to server
print("Connecting to the  server-1...")
socket = context.socket(zmq.REQ)
socket.connect ("tcp://localhost:5555")

while True:
    socket.send(b"request from client-2")

    #  Get the reply.
    message = socket.recv()
    print ("Received reply ", "[", message, "]")

    time.sleep(2)

