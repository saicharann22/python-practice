
import requests
from crypt import methods
from distutils.log import debug
import re
from tkinter.messagebox import RETRY
from flask import *
from flask import Flask,render_template,session,request,redirect,url_for
from pkg_resources import parse_requirements

app = Flask(__name__,template_folder='home2_html')

@app.route("/")   #routing 
def home():
    return render_template("home2.html")   #directing to home html

@app.route("/endapi2",methods = ["POST","GET"])
def endapi2():  
    if request.method == "POST": 
      
        city = request.form.get('city')  #getting values from form    
        key = "c954e276cfdeb69750c1cca9c366838a"  #unique to the user
        url = "https://api.openweathermap.org/data/2.5/weather?q="+city+"&appid="+key

        result = requests.get(url).json() #converting the object to json format
        name = result['name']
        p = result['main']['pressure']
        h = result['main']['humidity']
        wind_speed = result['wind']['speed']
        print(result)

        return render_template('endapi2.html',name = name,p =p,h = h,wind_speed= wind_speed)

if __name__ == '__main__':
    app.run(debug = True)

    
    