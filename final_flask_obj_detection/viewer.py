from xml.etree.ElementTree import TreeBuilder
import cv2
import zmq
import base64
import numpy as np
from PIL import Image
from flask_socketio import SocketIO, emit
import random


def viewer(frame):

    print("frames are coming from streamer")
    # context = zmq.Context()
    # footage_socket = context.socket(zmq.SUB)
    # footage_socket.bind('tcp://*:5555')
    # footage_socket.setsockopt_string(zmq.SUBSCRIBE, np.compat.unicode(''))
    # frame = footage_socket.recv_string()
    # print(frame)
    # while True:

    try:
        # Load Yolo
        net = cv2.dnn.readNet("yolov3.weights",  # algo model
                              "yolov3.cfg")  # algo config
        classes = []
        with open("coco.names", "r") as f:
            classes = [line.strip() for line in f.readlines()]

        layer_names = net.getLayerNames()  # names of the layers
        # get the index of the output layers(82,94,106)
        output_layers = [layer_names[i - 1]
                         for i in net.getUnconnectedOutLayers()]
        #colors = np.random.uniform(0, 255, size=(len(classes), 3))

        # receiving image socket via zmq
        # frame = footage_socket.recv_string()
        img = base64.b64decode(frame)
        npimg = np.frombuffer(img, dtype=np.uint8)
        source = cv2.imdecode(npimg, 1)

        img = cv2.resize(source, None, fx=1.2, fy=1.2)
        height, width, channels = img.shape

        blob = cv2.dnn.blobFromImage(img, 1/255,  # scale
                                     (416, 416),  # size of img to yolo
                                     (0, 0, 0),
                                     True, crop=False)

        net.setInput(blob)
        outs = net.forward(output_layers)

        # Showing informations on the screen
        class_ids = []
        confidences = []
        boxes = []
        for out in outs:
            for detection in out:
                scores = detection[5:]
                class_id = np.argmax(scores)
                confidence = scores[class_id]
                if confidence > 0.5:
                    # Object detected
                    center_x = int(detection[0] * width)
                    center_y = int(detection[1] * height)
                    w = int(detection[2] * width)
                    h = int(detection[3] * height)

                    # Rectangle coordinates
                    x = int(center_x - w / 2)
                    y = int(center_y - h / 2)

                    boxes.append([x, y, w, h])
                    confidences.append(float(confidence))
                    class_ids.append(class_id)

        indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)
        print(indexes)
        font = cv2.FONT_HERSHEY_PLAIN
        for i in range(len(boxes)):
            if i in indexes:
                x, y, w, h = boxes[i]
                label = str(classes[class_ids[i]])
                color = (255, 255, 255)  # colors[class_ids[i]]
                cv2.rectangle(img, (x, y), (x + w, y + h), color, 2)
                cv2.putText(img, label, (x, y + 30), font, 3, (0, 255, 0), 2)

        #cv2.imshow("Stream", img)

        # converting image to show in browser
        frame = cv2.resize(img, (800, 480))  # resize the frame
        encoded, buffer = cv2.imencode('.jpg', frame)
        jpg_as_text = base64.b64encode(buffer)

        print("pic converted to show in browser")

        # Saving the frame/image to the given folder
        im = Image.fromarray(img)
        file_name = "static/" + str(random.random()) + ".jpeg"
        im.save(file_name)
        emit('img64', file_name, broadcast=True)

        key = cv2.waitKey(30)

    except KeyboardInterrupt:
        cv2.destroyAllWindows()

    # cv2.destroyAllWindows()
