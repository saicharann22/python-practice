import base64
from inspect import currentframe
import math
from multiprocessing.sharedctypes import Value
from random import randrange
import random
from sys import _current_frames
import cv2
import zmq
import os
import time
import redis
import threading
from common import cache
from viewer import viewer
from flask_socketio import SocketIO, emit
import socket
global camera

current_frame = 0

def image_conversion():
    camera = cv2.VideoCapture(0)
    ret, frame = camera.read()
    frame = cv2.resize(frame, (800, 480))  # resize the frame
    encoded, buffer = cv2.imencode('.jpg', frame)
    jpg_as_text = base64.b64encode(buffer)
    # footage_socket.send(jpg_as_text)

    # saving the frames into  folder
    try:
        if not os.path.exists('frames_folder'):
            os.makedirs('frames_folder')
    except OSError:
        print('Error')

    if ret:
        name = './frames_folder/frame' + str(random.random()) + '.jpg'
        #print('captured...' + name)
        # cv2.imwrite(name, frame)
        frame = cv2.resize(frame, (800, 480))  # resize the frame
        encoded, buffer = cv2.imencode('.jpg', frame)
        jpg_as_text = base64.b64encode(buffer)
        viewer(jpg_as_text)
        #current_frame += 1


def streamer(value):
    print(value, 'streamer - 16')
    #a = main.get_status

    if value:
        print("done")
        context = zmq.Context()
        footage_socket = context.socket(zmq.PUB)
        footage_socket.connect('tcp://localhost:5555')

        # camera = cv2.VideoCapture(0)  # init the camera/video
        # ret,frame = camera.read()
        #value = redis_cache_memory.get("status")

        # while True:#camera.isOpened()
        #     #saving the frames into  folder
        #     try:
        #         if not os.path.exists('frames_folder'):
        #             os.makedirs('frames_folder')
        #     except OSError:
        #         print('Error')

        # sending the frame/image to detection(viewer.py)

        # current_frame = 0

        # while True:

        # ret,frame = camera.read()
        # frame = cv2.resize(frame, (800, 480))  # resize the frame
        # encoded, buffer = cv2.imencode('.jpg', frame)
        # jpg_as_text = base64.b64encode(buffer)
        # footage_socket.send(jpg_as_text)

        # cv2.imshow("Frame",frame)
        # key = cv2.waitKey(30)
        # if key == 27 or value == False:  #esc key
        #     break

        # if ret:
        #     name  = './frames_folder/frame' + str(current_frame) + '.jpg'
        #     #print('captured...' + name)
        #     cv2.imwrite(name,frame)
        #     current_frame += 1
        # else:
        #     break

        # camera.release()
        # cv2.destroyAllWindows()

    else:
        print("Not done")
        #camera = cv2.VideoCapture(0)
        camera.release()
        cv2.destroyAllWindows()
