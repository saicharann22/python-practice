# Opening the file in read mode
text = open("/home/neosoft/Desktop/python_practice/Subhash-Chandra-converted.txt", "r")

d = dict()

for line in text:

    line = line.strip()  #remove the spaces

    line = line.lower() #converting into lowercase

    words = line.split(" ")       # Split the line into words

    for word in words:

        if word in d:
            	# Increment count of word by 1 if already present in dictionary
            d[word] = d[word] + 1
        else:

            d[word] = 1   # if not present already, Add the word to dictionary with count 1

for key,value in list(d.items()):
    print("{}: {} ".format(key,value))
