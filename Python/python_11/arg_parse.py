import argparse

# creating a Parser object that  take all the info send from command line
parser = argparse.ArgumentParser(description ='Avg of given integers.')
  
# Adding Argument along  with the information about the arguements
parser.add_argument('integers',
                    metavar ='N',          #alternate name for the arguements is usage msgs
                    type = float,              #type to be converted 
                    nargs = '+',                #no of args to be taken
                    help ='an integer for the accumulator')
  
parser.add_argument('sum',     #name of the attribute to be return to object
                    action ='store_const',   #how the command line args should be handled
                    const = sum)
  n
parser.add_argument('len',
                    action ='store_const',
                    const = len)
  
args = parser.parse_args()   #parse_args stores all info into a varaible

add = args.sum(args.integers)

length = args.len(args.integers)
    
average = add / length

print("sum")
print(add)
print("length")
print(length)
print("average")
print(average)

#end
