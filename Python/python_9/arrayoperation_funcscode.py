#numpy operations and Ufuncs
import   numpy as np

a = np.array([1, 2, 3, 4])
b = np.array([5,6, 7, 8])

print("a = ",a)
print("b = ",b)

#arithmetic operations

difference = np.subtract(a, b)
print("difference: ", difference)

product = np.multiply(a, b)
print("product: ", product)

division = np.divide(a, b)  #whole quotient
print("division: ", division)

floor_division = np.floor_divide(a, b) #integer part of quotient
print("floor_division: ", floor_division)

remainder = np.mod(a, b)  #remainder
print("remainder: ", remainder)

a_power_b_with_function = np.power(a, b)
print("a_power_b_with_function: ", a_power_b_with_function)

a_power_b_with_operator = a**b
print("a_power_b_with_operator: ", a_power_b_with_operator)

#Relational operations

greater = np.greater(a,b)
print("a>b ; ",greater)

greater_or_equal = np.greater_equal(a,b)
print("a>=b  : ",greater_or_equal)

less = np.less(a,b)
print("a<b : ",greater)

less_or_equal = np.less_equal(a,b)
print("a<=b  ",less_or_equal)

equal = np.equal(a,b)
print("a==b ",equal)

not_equal = np.not_equal(a,b)
print("a!b : ",not_equal)

#bitwiese operations
a = np.array([1, 0, 1, 0, 1])
b = np.array([1, 0, 0, 1, 1])

print("np.bitwise_and(a, b):", np.bitwise_and(a, b))
print("               a & b:", a & b)

print("np.bitwise_or(a, b):", np.bitwise_or(a, b))
print("              a | b:", a | b, "\n")

print("np.bitwise_xor(a, b):", np.bitwise_xor(a, b))
print("               a ^ b:", a ^ b, "\n")
#-------------------------------------------
print("np.invert(a):", np.invert(a))
print("          ~a:", ~a, "\n")

print("np.left_shift(a, b):", np.left_shift(a, b))
print("             a << b:", a << b, "\n")

print("np.right_shift(a, b):", np.right_shift(a, b))
print("              a >> b:", a >> b, "\n")
