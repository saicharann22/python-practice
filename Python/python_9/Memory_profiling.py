import numpy as np
from memory_profiler import profile

# mprof -m run filename to --- profile the given function
#mprof plot  ----to plot the data into an image

@profile   #using decorator to specify to track the function
def function_1():
    a = np.ones((20,10,10))
    return a

fo = open("memory_report.log",'w+')
@profile(stream = fo)    #save the op in the file
def function_2():
    list_a = []
    for i in range(0,20):
        list_a.append(i)
    return list_a
    
function_1()
function_2()