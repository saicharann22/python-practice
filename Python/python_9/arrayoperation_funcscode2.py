import numpy as np

a = np.array([[1,2,3],
                    [4,5,6]])
   
b= np.array([[1,1,1],
                        [2,2,2]])   
    
print("a + b :",a+b)
print("a >b  :",a > b)

#--------------------

a  = np.array([[1,2,3,],
                      [4,5,6,]])  
    
b = np.array([[6,7],
                     [4,5,], 
                     [3,2]])
    
print(a.shape)   #2*3   shape
print(b.shape)    #3*2  shape

mat_multi = np.matmul(a,b)
print(mat_multi)                     

a = np.array([1,2,3,4])
b = np.array([5,6,7,8])   #shape = (4,)

print(a.shape)
print(b.shape)

print(np.matmul(a,b))

a = a.reshape(1,4)  #reshaping
b = b.reshape(4,1)

print(np.matmul(b,a))

print(a.shape)
t = np.transpose(a)
print(t)  #transpose
print(t.shape)

t2 = b.T
print(t2)


a = np.array([3, 1, 4, -2]).reshape(1,4 )
print("Array before transposing: \n", a)

b = np.transpose(a)
print("Array after transposing: \n", b)


a = np.array([3, 1, 4, -2])
print("Rank 1 Array before transposing: \n", a)

b = np.transpose(a)
print("Rank 1 Array after transposing: \n", b)


a = np.array([[1,2],
                      [1,3]])
print("Determinant :",np.linalg.det(a))
print("MultiInverse :",np.linalg.inv(a))  #Multiplicative inverse





