#Numpy index and slicing

import numpy as np

a = np.array([1,2,3,4,5])   #creating an array
print(a)

third_position_value = a[3]   #finding 3rd position value
print(third_position_value)

slicing  = a[0:3]   #slicing
print(slicing)
print()

print(" reff array")
print(a)                                              
negative_slicing = a[-4:-1]      #slicing
print(negative_slicing)


step_index = a[::2]         #step_index
print(step_index)


omit_start_stop_indices = a[::]   #without start and stop indices
print(omit_start_stop_indices)


negative_step_slicing = a[::-2]      #negative_step_slicing
print(negative_step_slicing)


a = [5,6,7,8,9,6,4]                      #negative_slicing
negative_step_slicing2 = a[3:-1:-1]
negative_step_slicing3 = a[-1:0:2]
print(negative_step_slicing2)
print(negative_step_slicing3)


modify_b = np.array([1,4,5,3,6,7,4,3,5,9])      #modify
print(modify_b)
modify_b[2:5] = 6
print(modify_b)


a[:4] = np.array([3,4,5,6])    #modify with right_shape 
print(a)
print("----')

print(a)
sliced_a = a[:4]
sliced_a[0]  = -5
print(sliced_a)

a = np.array([1,2,2,3,56,7])
b = a

b[0] = 111
b[-1] = 222
print(a)
print(b)


#using copy
a = np.array([1,2,2,3,56,7])
b = a.copy()

b[0] = 444
b[-1] = 555
print(a)
print(b)


































 