import numpy as np

#with 2D arrays
a_2d = np.array([['A','B','C'],
                                ['D','E','F'],
                                ['G','H','I'] ] )

print(a_2d[0,0]) #accessing elements 
print(a_2d[1,2])

print(a_2d[1])   #accesiing single row
print(a_2d[:,2])  #accesiing  sinlge column


print(a_2d[1:3])  #slicing 2d array
print(a_2d[1:,1:])  


b_2d =np.array([['A','B','C','D'],
                                ['E','F','G','H'],
                                ['I','J','K','L'] ] )
                                
                                
print(b_2d[:,0:4:2])  #using step 

sliced_2d = b_2d[1:3,:]
sliced_2d[0]= 'k'
print(sliced_2d)

new_array = np.array([2,3,4,5,6,7,8])  #fancy Indexing
index_array = np.array([0,2,4])
indexed_array = new_array[index_array]
print(indexed_array)


#Boolean indexing should match with the shape of original array
mask_array = np.array([True,False,True,False,True,False,True])
print(new_array[mask_array])



#fancy indexing in 2d array
array_2d =np.array([['A1','B1','C1','D1'],
                                ['E2','F2','G2','H2'],
                                ['I3','J3','K3','L3'] ] )
                                
#rows = array_2d[0:3:2,0]
 
rows = array_2d[(0,2),0]    #one way of selecting 0,2 rows
print(rows)                                     #     with oth column position 
                    
only_rows = array_2d[[1,2]]   #getting only rows
print(only_rows)                        

s_columns = array_2d[0,(0,3)]   #selecting particular columns
print(s_columns)

result1 = array_2d[[0]]
print("result1 - ",result1)

result2 = array_2d[[0,2]]
print("result2 - -",result2)

result3 = array_2d[ [0,2] ] [:,  [0,2] ] 
print("result3 - ",result3)

result4 = array_2d[(2,0),(0,3)]  #particular position
print("result4",result4)

array_2d =np.array([['A1','B1','C1','D1'],
                                ['E2','F2','G2','H2'],
                                ['I3','J3','K3','L3'] ]

row_mask = np.array([False,True,True]) 
col_mask = np.array([True,False,True,False])
r5 = array_2d[row_mask,3]  #in 3 rd column gives o/p based on row_mask
r6 = array_2d[row_mask,:]

r7 = array_2d[2,col_mask]  # in 2 row gives o/p based on col_mask


print(r5)
print(r6)
print(r7)

a = np.array([1,2,3])
a[0] = np.array([2,5])
print(a)