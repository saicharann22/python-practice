import multiprocessing
import os

result1= []  #list out of  function(global scope)
  
def cubing(numlist):
    for each in numlist:
        result1.append(each **3)
    print("Result 1  in process p1 is " ,result1)
    print()
def cubing2(numlist2,result2,v):
    v.value = 3.8
    for idx,each  in enumerate(numlist2):
        result2[idx] = (each **3)
    print("Result 2 in process p2 is " ,result2[:])
    print()
    print("value in process p2 is " ,v.value)
    print()
    

list_a= [1,2,3,4] 
p1 = multiprocessing.Process(target=cubing, args=(list_a,))

list_b = [5,6,7]
v = multiprocessing.Value('f',0.0)
result2= multiprocessing.Array('i',3)
p2 = multiprocessing.Process(target = cubing2,args=(list_b,result2,v,))

p1.start()   # starting process_1
p1.join()  #halting the main process to let sub process to  complete 

p2.start()
p2.join()

# print global result list
print("Result  1 in main program ",result1)
print()
print("Result 2 in main program ", result2[:])
print()
print("value in main program ", v.value)
print()
#end

