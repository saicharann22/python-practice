#multithreading2

import os
import threading  

def function1(n):  
    print("The current temperature is ", n)  

t1 = threading.Thread( target = function1, args =(45, ))  

t1.start()  #started thread
t1.join()


print(os.getpid())   #id of current process

print(threading.main_thread())   #main thread
