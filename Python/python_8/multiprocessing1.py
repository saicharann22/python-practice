#multiprocessing  

import multiprocessing
import os

def process_1():
    id1  = os.getpid()   # ID of current working process_1
    print("current working process_1 Id = ",id1)
    print()

def process_2():
    id2  = os.getpid()   # ID of current working process_2
    print("current working process_2 Id = ",id2)
    print()

p1 = multiprocessing.Process(target = process_1)
p2 = multiprocessing.Process(target = process_2)

p1.start()   # starting the process_1
p2.start()   #starting the process_2

print("Under working of processs ") 
print()
print("alive_status of  p1",p1.is_alive())

print()
print("ID of the process_1",p1.pid)
print("ID of the process_2",p2.pid)

p1.join()  #halting the main processs to execute after completion of process_1
p2.join()  #halting the main process to execute after completion of process_2

print("After execution of processs ")  
print("alive_status of  p1",p1.is_alive())   
 

