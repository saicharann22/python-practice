#multithreading

import threading
from threading import *
from time import sleep
import time

def func_1(name1):
    print("Good Morning " + name1)
    sleep(1)       #using sleep to avoid collison during simultanous execution
   
def func_2(name2):
    print("Good Evening " + name2)
    sleep(1)
    
def func_3(list_a):
    for each in list_a:
        print("square of  "+ str(each) + " is"  ,each**2)
        
list_a = [1,2,3,4]
t = time.time()
t1 = threading.Thread(target = func_1,args = ("raghu",)) #creating a thread
t2 = threading.Thread(target = func_2,args = ("ram",))
t3 = threading.Thread(target = func_3,args = (list_a,))

t1.start()   # starting a thread1
t2.start()   #starting a thread 2
t3.start()   #starting a thread 3

t1.join()  #halting the main thread to execute after completion of thread1
t2.join()  #halting the main thread to execute after completion of thread2
t3.join()  #halting the main thread to execute after completion of thread3

print("totaltime",time.time())
print(t1.is_alive())
print("Ending")  #executed by main thread
