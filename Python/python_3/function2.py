#yield

def func2():
    i = 1
    while i<=3:
        yield i
        i = i + 1

f= func2()
print(list(f))


#decorators

def div(a,b):
    print(a/b)

def smart_div(func):

    def inner(a,b):
        if a < b:
            a,b = b,a
            return func(a,b)
    return  inner

div(2,4)
div = smart_div(div)
