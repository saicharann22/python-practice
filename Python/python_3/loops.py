
# enumerate

for i in range(1,4):
    if i == 3:
        continue
    print(i)

print("charan")


for key, value in enumerate(['The', 'Big', 'Bang', 'Theory']):
    print(key, value)


#------------------------------
#zip

questions = ['name', 'colour', 'shape']
answers = ['apple', 'red', 'a circle']
a  = list(zip(questions,answers))
print(a)

#for key, value in zip(questions, answers):
#    print('What is your {0}?  I am {1}.'.format(key, value))
