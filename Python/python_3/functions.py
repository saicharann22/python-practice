#*args
def my_fun(*argv):
    a1 = argv
    print(a1)
    #for arg in argv:
    #    print(arg)
my_fun('Hello', 'Welcome', 'to', 'Neosoft')


#Docstring

def even_odd(x):
    """Function to check if the number is even or odd"""
    if x % 2 == 0:
        print("even")
    else:
        print("odd")


# Driver code to call the function
print(even_odd.__doc__)
even_odd(2)
