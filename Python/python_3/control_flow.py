#fibonacci

def fib(n):
    a = 0
    b = 1
    while a < n:
        yield a
        a,b = b , a+b

f  = fib(5)
print(list(f))

#-------------------------
#lambda

c = [1,2,3,4]

mapped = map (lambda x: x**2, c)
print(list(mapped))
#-----------------
