import threading
 
x = 0
def increment():
    global x
    x += 1
  
def thread_func(lock):    #using lock as an arguement to the function
 
    for _ in range(100000):
        lock.acquire()                #using lock methods to avoid race condition
        increment()
        lock.release()
  
def main_function():
    global x
   
    x = 0    # setting global variable x as 0
  
    lock = threading.Lock()    # creating a lock
  
    t1 = threading.Thread(target=thread_func, args=(lock,))   # creating thread and passing lock as a arguement 
    t2 = threading.Thread(target=thread_func, args=(lock,))
  
  
    t1.start()     # start threads
    t2.start()
  
    
    t1.join()   # wait until each thread comlete each function
    t2.join()
  

for i in range(10):
    main_function()
    print("Iteration {}: x = {}".format(i,x))  
