#Logging
import logging
import sys

logging.basicConfig(filename="logfile.log",                                             
                              format='%(asctime)s %(message)s',           # Create and configuring logger
                              filemode='w',
                              datefmt = '%Y-%m-%d  %H : %M : %S' )
 

logger = logging.getLogger()   # Creating an object
 
logger.setLevel(logging.CRITICAL)  # setting Threshold level

# Test messages
for i in range(0,15):

    if (i%2 == 0):
        logging.warning('Warning Message')  #severity level
    elif(i %3 == 0):
        logging.critical('critical Message')
    else:
        logging.error('error Message')
     
    
a = 4
b = 0
try:
    c = a/b
except Exception as e:
    logging.error('Exception  occured ',exc_info = True)   #traces of error (exec_info) logged into logfile
    print(sys.exc_info())   #print detailed traces of error
    
    #end
