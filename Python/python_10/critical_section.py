import threading

x = 0       # global variable x
def increment():

    global x
    x += 1
  
def thread_func():   #recursive

    for i in range(100000):   #critical section taking place
        increment()                  #increment function called inside thread_task
  
def main_function():
    global x                          # setting global variable x as 0
    x = 0
  
    t1 = threading.Thread(target=thread_func)   # creating threads
    t2 = threading.Thread(target=thread_func)
  
    t1.start()    # start threads
    t2.start()
  
    t1.join()       # wait until threads finish their job
    t2.join()

for i in range(10):
    main_function()
    print("Iteration {0}: x = {1}".format(i,x))
    
    #end
