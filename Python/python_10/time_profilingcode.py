import time    # importing time module

from  line_profiler  import LineProfiler

import cProfile

def func_1():
    start = time.time()
    print("Time consumed")
    end = time.time()  
    print(" Time take by function ", end-start, "seconds")

def func_2():
    for i in range(1,5):
        list_a =[]
        list_a.append(i)

def f():
    print("hello")

print("--------fucntion_1------")
func_1()    # Calling fucntion

print("-------fucntion_2------")
L_time = LineProfiler(func_2)
L_time.print_stats()    
print("-------fucntion_3------")

cProfile.run('f()')

#end
