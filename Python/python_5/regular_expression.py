
import re

string_a = "Har har shiva shivaaaya namaha"
x =  re.findall("sh",string_a)   #using findall for all occurences in list
print(x)

y = re.search("har",string_a) # using search function
print(y.start())	 #using start method for match object
print(y.span())	#using span method for match object
print(y.string)        #using string method for match object

z = re.split("h",string_a) #using split method to split at that pattern
print(z)

w = re.sub("a","&",string_a)  #using sub to replace a with &
print(w)
#end
