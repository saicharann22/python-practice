class Car:
    def __init__(self,name,mileage):
        self._name = name
        self.mileage = mileage
    def display(self):
        print(self._name)
        print(self.mileage)


class Car2(Car):          #inheritance
    pass

obj1 = Car("maruthi",15)
obj2 = Car2("alto",5)
obj2.display()
#obj2.display_details(25)
#def display_details(self,age):
        #super().display()
        #self.age = age
        #print(self.age)
