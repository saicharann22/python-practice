from abc import ABC,abstractmethod

#polymorphism method overloading
'''
class Demo():
    def add(self,A,B= 10,C= 20):
        print(A)
        print(A+B)
        print(A+B+C)
d = Demo()
d.add(10)
d.add(20,30)
'''

class Abstraction(ABC):   	 #Abstract Class
    @abstractmethod
    def housing_interest(self):  #Abstract Method
        pass
    @abstractmethod	         #Abstract Method
    def vehicle_interest(self):
	    pass
class SBI(Abstraction):          #concrete class

    def housing_interest(self):  #Method overriding
        print("HousingInterest")
    def vehicle_interest(self):
        print("vehicle")

a = SBI()
a.housing_interest()
