import numpy as np   # importing numpy
                                    #import numpy as np
print("Hello World!")

# Creating NumPy Arrays

np_array_from_list = np.array([1, 8, 3, 6, -4])
print(np_array_from_list)

typecasted_float_array = np.array([1, 8, 3, 6, -4], dtype=float)
print("typecasted_float_array:", typecasted_float_array)

int_array = np.array([1, 8, 3, 6, -4], dtype=np.int16)
print("int_array:", int_array)

typecasted_int_array = np.array([1, 8, 3, 6, -4], dtype=np.int64)
print("typecasted_int_array:", typecasted_int_array)


p = np.power(100, 8, dtype=np.int64)
print(p)
p2 = np.power(100, 8, dtype=np.int32)
print(p2)


# dtype - Precision

np.array([1.00001, 1.00001], dtype=np.float16) #gives 16bit decimal number

np.array([1.00000022, 1.00000022], dtype=np.float32) #gives  float 32bit number

np.array([1.00000022, 1.00000022], dtype=np.float64) #gives  float 64bit number

# Creating 2D Numpy Arrays

array_2d = np.array([[1, -2, 0],
                     [3, 4, 2],
                    [3, 8, 9]])

#size

print(array_2d.size) #no of elements in the array (m*n)

# ndim

print(array_2d.ndim)

#shape attribute:

array_2d = np.array([[1, -2, 0],
                     [3, 4, 2]])
print(array_2d.shape)

print(array_2d.dtype)
