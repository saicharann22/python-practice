import numpy as np
# Creating NumPy Arrays with Constant Values

#no.ones

ones_5 = np.ones(5)   #creates an array of ones
print(ones_5)
print("----------")

ones_2d = np.ones([3,4])
print(ones_2d)
print(ones_2d.dtype)
print("----------")

ones_2d = np.ones([2,3], dtype=np.int32)
print(ones_2d)

# np.zeros

a = np.zeros(2)               #creates an array of zeros
print(a)
print("------")
b = np.zeros([3,4],dtype = np.int64)
print(b)
print("------")
c = np.zeros((5,3,4))
print(c)
print(c.ndim)


zeros_5 = np.zeros(5)
print("zeros_5: ", zeros_5)

zeros_2d = np.zeros([3,4], dtype=np.int32)
print("zeros_2d: \n", zeros_2d)

# np.full

fill_array = np.full([2,3], fill_value=6.19)   ##creates an array with values given to fill
print(fill_array)
print("--------")
int_array = np.full(10, fill_value=3.2, dtype=int)
print("int_array: ",int_array)
print("--------")
bool_array = np.full(6, fill_value=False)
print("bool_array: ",bool_array)
print(bool_array.dtype)


# np.empty()

empty_array = np.empty(4, dtype=int) # takes radom garbage value
print(empty_array)

empty_2d = np.empty([2,4], dtype=np.float32)
print(empty_2d)

# Identity matrix

identity_matrix = np.eye(5) #square matrix
print(identity_matrix)

# Diagonal Matrix

diagonal_matrix = np.diag([1, 2, 3, 5, 6, 7])
print(diagonal_matrix)
