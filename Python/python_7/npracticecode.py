import numpy as np

a = np.array([1,2,3])
print(a)
print("-----------------------")

size = a.size
print(size)
print("-----------------------")

aray = np.array([[1,2,3],
            [4,5,6]])

ndim = aray.ndim
print(ndim)
print("-----------------------")

shape = aray.shape
print(shape)
print("-----------------------")

ones = np.ones(3,dtype = int)
ones1 = np.ones([3,4])
print(ones)
print(ones1)
print(ones1.dtype)
print("-----------------------")

zeros = np.zeros((2,3,4))
print(zeros)
print("-----------------------")


full= np.full([3,5],3)
print(full)
print("-----------------------")

empty = np.empty(4,dtype= int)
print(empty)
print("-----------------------")

identity = np.eye(4) #square matrix
print(identity)
print("-----------------------")

diagnol = np.diag([1,2,3,5]) #square matrix with -
print(diagnol)                       #-given values as diagnol
print("-----------------------")

arange1 = np.arange(2,3,step =0.1) #end = true is not valid
print(arange1)             #start ,stop ,step   precise step values
print("-----------------------")


linspace = np.linspace(2,6,num= 20,retstep = True)  #default 50 values
linspace2 = np.linspace([1,2,3],[4,5,6],num = 5)  #precise endvalues
linspace3 = np.linspace(0,3,4)
print(linspace)
print(linspace2)
print(linspace3)
print("-----------------------linspace")


geomspace = np.geomspace(2,5,num = 10)
print(geomspace)
print("-----------------------")

logspace  = np.logspace(2,5,base=2,num = 10)
print(logspace)
print("-----------------------")


random = np.random.random(4)
print(random)#end
