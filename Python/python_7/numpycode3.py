import numpy as np
# NumPy Sequential Arrays

# np.arange  #importance is for incremental values

numbers_array = np.arange(1,10)
print(numbers_array)

numbers_start_and_stop = np.arange(2,8) #using start and stop
print(numbers_start_and_stop)

numbers_using_step = np.arange(start=1,
		              	stop=12, step=2)  #using step
print(numbers_using_step)

numbers_with_neg_step = np.arange(20, 10, -1) #using negative step
print(numbers_with_neg_step)

numbers_with_float_step = np.arange(start=9,
				     stop=10, step=0.1111111111111) #using float step
print(numbers_with_float_step)

# np.linspace

seq_array = np.linspace(start=2, stop=6, num=5)  #linear spacing
print(seq_array)

seq_array_without_num = np.linspace(start=4.0, stop=6.0)  #50 default values
print(seq_array_without_num)

seq_array_excluding_endpoint = np.linspace(start=4.0,
			  	             stop=6.0, num=5, endpoint=False)
print(seq_array_excluding_endpoint)

seq_array_with_retstep = np.linspace(start=4.0, stop=8.0,
				      num=5, endpoint=False,
				      retstep=True) #evenlyspaced
print(seq_array_with_retstep)   #retstep gives value of spacing

seq_2d_array = np.linspace(start=[2, 3, 5], stop=[4, 6, 7], num=5)
print("linspace array in 2D: \n", seq_2d_array)

seq_2d_array_axis_1 = np.linspace(start=[2, 3, 5],
				   stop=[4, 6, 7], num=5, axis=1)
print("linspace array in 2D along axis 1: \n", seq_2d_array_axis_1)

geo_space = np.geomspace(start=1, stop=128, num=8) #geometric progression
print(geo_space)

log_space = np.logspace(start=2, stop=10, num=5, base=2)
print(log_space)

# np.random.random()

random_valued_array = np.random.random(4)     #random values btwn 0 - 1
print(random_valued_array)

random_2d = np.random.random([3,2])
print(random_2d)
