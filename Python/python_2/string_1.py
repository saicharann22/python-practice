import string

# printing in hex
STRING1 = "This is \x48\x65\x65\x6b\x73 in \x48\x45\x58"
print("\nPrinting in HEX with the use of Escape Sequences: ")
print(STRING1)

# Using raw String to
# ignore Escape Sequences
STRING1 = r"This is \x48\x65\x65\x6b\x73 in \x48\x45\x58"
print("\nPrinting Raw String in HEX Format: ")
print(STRING1)

# Formatting of Integerss
STRING1 = "{0:b}".format(16)
print("\nBinary representation of 16 is ")
print(STRING1)
# --------------------------------------------------------------
# Formatting of Floats
STRING1 = "{0:e}".format(165.6458)
print("\nExponent representation of 165.6458 is ")
print(STRING1)

# Rounding off Integers
STRING1 = "{0:.2f}".format(1/6)
print("\none-sixth is : ")
print(STRING1)
#------------------------------------------------------------
# String alignment
STRING1 = "|{:<10}|{:^10}|{:>10}|".format('Geeks', 'for', 'Geeks')
print("\nLeft, center and right alignment with Formatting: ")
print(STRING1)

# To demonstrate aligning of spaces
STRING1 = "\n{0:^16} was founded in {1:<4}!".format("GeeksforGeeks", 2009)
print(STRING1)


A = string.ascii_letters
print(A)

B = chr(112)
print(B)

C = [1,2,3]
C.extend([5])
print(C)
