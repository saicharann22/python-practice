
A = [1,2,3]
A.extend([5])
print(A)


Dict = { 5 : 'Welcome', 6 : 'To', 7 : 'Neosoft',
        'A' : {1 : 'Hi', 2 : 'hello', 3 : 'Neosoft'},
        'B' : {1 : 'IT', 2 : 'Life'}}
print("Initial Dictionary: ")
print(Dict)

# Deleting a Key value
del Dict[6]
print("\nDeleting a specific key: ")
print(Dict)

# Deleting a Key from
# Nested Dictionary
del Dict['A'][2]
print("\nDeleting a key from Nested Dictionary: ")
print(Dict)
print("hello")
