# shallow copy and deep copy
import copy
list1=[1,2,3,4,5]
list2=list1
print(list1)
print(list2)

l1=[10,20,[30,40],50]
l2=copy.copy(l1)
l1[2][0]=888
l1[1]=200
print(l1)
print(l2)
#it will create new and independent object with same content
print(id(l1))
print(id(l2))
print(id(l1[2][0]))
print(id(l2[2][0]))
#when we change any nested objects in old_list, the changes appear in new_list.
#--sboth lists share the reference of same nested object
l1=[10,20,[30,40],50]
l2=copy.deepcopy(l1)
# completed duplicate object will be created including nested object
## if we perform any change that doesnot show in duplicate copy
l1[2][0]=786
print(l1)
print(l2)
print(id(l1))
print(id(l2))
print(id(l1[2][0]))
print(id(l2[2][0]))
#end
