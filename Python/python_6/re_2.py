import re
string_b = "hello all welcome to the world"
a = re.findall("[loa]",string_b)  #takes charectar by charectar
print(a)


b = re.findall("^wel",string_b)   #startswith wel as a word not as a charectar
print(b)

c = re.findall("world$",string_b)  #ends with given word
print(c)

d = re.findall("welco..",string_b)   #checks for all chars in .. except new line
print(d)

e  =re.findall("..l",string_b)
print(e)

string_c = "123 friend in need is friend in deed"

f = re.findall("ee*",string_c)    #zero or one occurances
g = re.findall("e*",string_c)

print(f)
print(g)


h = re.findall("ie+",string_c)  #one or more occurances
print(h)

i = re.findall("ie{2}",string_c) #occurences for specified time in {}
print(i)


j = re.findall("\s",string_c)  #return match having spaces
k = re.findall("\S",string_c)  #return match  not having any spaces
print(j)
print(k)

l = re.findall("\d",string_c)   #having digits 0-9
m = re.findall("\D",string_c) #not having any digits
print(l)
print(m)

n = re.findall("\w",string_c)  # returns all word charectars
o = re.findall("\W",string_c) #return all non-word charectars
print(n)
print(o)


p = re.findall("[123]",string_c)
q = re.findall("[^123]",string_c)
r = re.findall("[0-9]",string_c)
s = re.findall("[0-9][0-9][0-9]",string_c)
print(p)#end
print(q)
print(r)
print(s)
