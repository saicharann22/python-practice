from crypt import methods
from distutils.log import debug
import re
from tkinter.messagebox import RETRY
from flask import *
from flask import Flask,render_template,session,request,redirect,url_for
from pkg_resources import parse_requirements

app = Flask(__name__,template_folder='Home_html')

@app.route("/")   #routing 
def home():
    return render_template("home.html")   #directing to home html

@app.route("/endapi",methods = ["POST","GET"])
def endapi():  
    if request.method == "POST": 
        #session["a"] = "a"
        #session['b'] = "b"
        a = int(request.form.get('a')) #getting values from form  
        b = int(request.form.get('b'))  
        #a = int(a)
        #b = int(b)
        print(type(a))   

    # return redirect(url_for('ending'))
    #a = session.get('a')
    #b = session.get('b')
    
    #a = request.args.get('a',None)
    #print(a)
        return render_template('endapi.html',a = a,b = b)

if __name__ == '__main__':
    app.run(debug = True)

    
    