import pandas as pd

#cars_df = pd.read_csv("/home/neosoft/Desktop/python_practice/practices/pandas/cars.csv")   #reading the data
cv_df = pd.read_csv("/home/neosoft/Desktop/python_practice/practices/pandas/archive/covid_19_india.csv")   #reading the data


df = pd.DataFrame(cv_df)
#print(df)


print("shape :",df.shape)  #no of rows and columns
print("columns :",df.columns)  # no of columns along with index

print(df.head(n=5))  #gives first 5 values of data 
print(df.head(-100)) #excludes last 100 values in dataframe

print(df.tail(n= 6)) # gives bottom 6 valuess
print(df.tail(-100))  #excludes top 100 values

print("Describe method--------------")     
print(df.describe())  #gives descriptive statistics

print("Dsescribe with object inlcuded--------------")
print(df.describe(include = [object]))

print("Describe including all---------- ")
print(df.describe(include = "all"))

print("info-------------")

print(df.info())  #summarising the addtional info of the dataframe


people = {                                                  #dictionary of lists
    "firstname": ["Sam", 'Max', 'Jeff'],             #maintain same list length  
    "lastname": ["Carol", 'Willianms', 'Shan'], 
    "email": ["sammyC@gmail.com", 'Maxie.Williams@email.com', 'JeffShan@email.com']
}

df2 = pd.DataFrame(people)   #creating a data frame using dict of list
print(df2)

print(people['firstname'])            #gets a list of values

print(df2['firstname'])
print(df2['lastname'])     #gets a series of values along with index

print(type(df2['lastname']))


print(pd.Series([1,2,3,4]))    #series is a list of data with additional functionalitys

print(df.columns)

print(df['Date'][250])  #indexing

print(df.iloc[[0,2],[0,3]])
print(df.iloc[1,5])
   
print("-----")
print(df.loc[200])
 
print(df.loc[ [0,3] , ['Date','Cured'] ] )   #can also specify the name of the column instead of index
 
print(df.at [3000,'Cured'])  #values at particular location



print(df[0:3])

#major diff btwn loc and normal slicing is "end indices" is included in loc slicing 

print(df.loc[0:3])
print("--------------")

print(df.columns)
print(df.loc[4:7,'Date':'Cured'])# end
