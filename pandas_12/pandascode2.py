import pandas as pd

#cars_df = pd.read_csv("/home/neosoft/Desktop/python_practice/practices/pandas/cars.csv")   #reading the data
cv_df = pd.read_csv("/home/neosoft/Desktop/python_practice/practices/pandas/archive/covid_19_india.csv")   #reading the data


df = pd.DataFrame(cv_df)
print(df)   

setting display of row 
pd.set_option('display.min_rows',4)  #setting min no of rows
pd.set_option('display.max_columns',5)   #setting max of columns

pd.reset_option('display')   #to reset to default (10)
print(df)


index
print(df.index) # to get the range of start,stop and step of the dataframe
print(df.columns)
print(df.set_index('Sno'))

print(df)

print(df.set_index('Sno', inplace = True))   # inplace is used to make the sno
                                                                                    as the index for the original df

pd.read_csv("filepath",index_col = 'Sno') # to set the req col as index at start

df.reset_index()  # to reset the default index instead of seleceted one
df.reset_index(drop = False,inplae  = False)  #drop tells whether to reset index to original or not


changing the  index column implies changing the value while using loc to access the data


sorting based on index
df.set_index('Sno',inplace = True)
df.sort_index(ascending = False,inplace = True)   #print from last to first in descending order
print(df)

filter = df['Cured'] > 1000
print(df.loc[filter,'State/UnionTerritory'])  # using filtering to find cured states > 1000s

range_filter = (df['Cured'] > 1000) & (df['Deaths'] < 500)  #using logical operators to find the desired o/p
print(df.loc[range_filter])

string_filter = df['State/UnionTerritory'].str.contains('Maharashtra')   #filtering using str method
print(df[string_filter])