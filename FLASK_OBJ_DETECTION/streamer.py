import base64
import cv2
import zmq
import os
import time

context = zmq.Context()
footage_socket = context.socket(zmq.PUB)
footage_socket.connect('tcp://localhost:5555')

camera = cv2.VideoCapture("video_folder/5.mp4")  # init the camera/video

while camera.isOpened():

    #saving the frames into  folder
    try:
        if not os.path.exists('frames_folder'):
            os.makedirs('frames_folder')
    except OSError:
        print('Error')

    #sending the frame/image to detection(viewer.py)
    current_frame = 0
    while True:
        ret,frame = camera.read()
        frame = cv2.resize(frame, (800, 480))  # resize the frame
        encoded, buffer = cv2.imencode('.jpg', frame)
        jpg_as_text = base64.b64encode(buffer)
        footage_socket.send(jpg_as_text)

        #cv2.imshow("Frame",frame)
        key = cv2.waitKey(30)

        if key == 27:  #esc key
            break

        if ret:
            name  = './frames_folder/frame' + str(current_frame) + '.jpg'
            print('captured...' + name)
            cv2.imwrite(name,frame)
            current_frame += 1
        else:
            break

    camera.release()
    cv2.destroyAllWindows()

'''
    try:
        grabbed, frame = camera.read()  # grab the current frame
        frame = cv2.resize(frame, (800, 480))  # resize the frame
        encoded, buffer = cv2.imencode('.jpg', frame)
        jpg_as_text = base64.b64encode(buffer)
        footage_socket.send(jpg_as_text)

    except KeyboardInterrupt:
        camera.release()
        cv2.destroyAllWindows()
        break

camera.release()
cv2.destroyAllWindows()

'''


