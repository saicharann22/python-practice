import re
from xml.dom.xmlbuilder import DocumentLS
import flask
from flask import Flask, render_template, request
import redis

app = Flask(__name__,template_folder = "flask_obj_detect.html")

# connecting to redis database
redis_cache_memory = redis.Redis(host='localhost',
                                 port=6379,
                                 db=2,
                                 charset = "utf-8",
                                 decode_responses=True)

@app.route("/")
def home():
    return render_template("index.html")

@app.route('/on_off',methods = ["GET","POST"])
def on_off():
    value = request.json.get("key")
    print(value)
    if value:
        return "ON"
    else:
        return "OFF"

if __name__ == '__main__':
    app.run(debug= True)


