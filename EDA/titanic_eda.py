#importing libraries

# Commented out IPython magic to ensure Python compatibility.
import pandas as pd
#import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import preprocessing
#from sklearn import preprocessing
# %matplotlib inline

#reading the data

train_dataset = pd.read_csv("train.csv")
test_dataset = pd.read_csv("test.csv")

#train_dataset

#statistical info

train_dataset.describe()

##Knowing number of  null values present in the each column

train_dataset.isnull().sum()



#ploting the null values using seaborn

sns.heatmap(train_dataset.isnull(),xticklabels = True,yticklabels = False,cmap = "YlGnBu")

###Ploting for clear understating

sns.set_style('whitegrid')
sns.countplot(x='Survived',data=train_dataset)

sns.set_style('whitegrid')
sns.countplot(x='Survived',hue = 'Sex',data=train_dataset)

sns.set_style('whitegrid')
sns.countplot(x='Survived',hue ='Pclass',data=train_dataset)

sns.scatterplot( x="Age", y='Pclass', data=train_dataset,
                hue='Sex')

##Droping and replacing values
#Cabin and Ticket values do not make an impact to the output
train_dataset.drop(['Cabin','Ticket'],axis = 1,inplace = True) 

#train_dataset

sns.heatmap(train_dataset.isnull(),xticklabels = True,yticklabels = False,cmap = "YlGnBu")

sns.histplot(x='Age', data=train_dataset, )
plt.show()

#people above age 60 are less

train_dataset.boxplot(column=['Age'])
plt.show()

sns.countplot(x='SibSp',data=train_dataset)

#Handling the Null values

age_replace = train_dataset['Age'].median()
#age_replace

train_dataset['Age'] = train_dataset['Age'].fillna(value = 28)

train_dataset.isnull().sum()

def male_female_child(passenger):
    age, sex = passenger

    if age < 16:
        return 'child'
    else:
        return sex

train_dataset['Person'] = train_dataset[['Age', 'Sex']].apply(male_female_child, axis=1)
train_dataset.head(50)

train_dataset.drop('Fare',axis=1,inplace=True)


label_encoder = preprocessing.LabelEncoder()
train_dataset["Embarked"] = label_encoder.fit_transform(train_dataset["Embarked"])
train_dataset


## Number of passengers from each city

train_dataset.groupby('Embarked')['Embarked'].count()

###Handling null values in Embarked

train_dataset["Embarked"] = train_dataset["Embarked"].fillna(value = "S")

train_dataset.isnull().sum()

#train_dataset


le = preprocessing.LabelEncoder() #
train_dataset["Sex"] = le.fit_transform(train_dataset["Sex"])
#train_dataset

correlation = train_dataset.corr()

sns.heatmap(correlation,
	    xticklabels =correlation.columns,
	    yticklabels = correlation.columns,
	    annot = True )
