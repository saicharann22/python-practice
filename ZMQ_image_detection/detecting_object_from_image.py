import base64
from calendar import c
import cv2
import numpy as np

import zmq
context = zmq.Context()
#  Socket to talk to server

print("Connecting to  server-1…")
socket = context.socket(zmq.REP)
socket.connect("tcp://localhost:5555")


#####################
# Load Yolo
net = cv2.dnn.readNet("yolov3.weights", #algo model
                      "yolov3.cfg")  #algo config
classes = []
with open("coco.names", "r") as f:
    classes = [line.strip() for line in f.readlines()]

layer_names = net.getLayerNames() #names of the layers
#get the index of the output layers(82,94,106)
output_layers = [layer_names[i - 1] for i in net.getUnconnectedOutLayers()]
colors = np.random.uniform(0, 255, size=(len(classes), 3))


# Loading image
message = socket.recv() #receives byte array

temp_img = open("sample.jpg", 'wb') #dummy img
temp_img.write(message) #inserting byte array
temp_img.close()

img = cv2.imread("sample.jpg")

img = cv2.resize(img, None, fx=0.3, fy=0.25)
height, width, channels = img.shape

# Detecting objects(extracting features from img)
blob = cv2.dnn.blobFromImage(img, 1/255, #scale
                             (416, 416), #size of img to yolo
                             (0, 0, 0),
                             True, crop=False)

net.setInput(blob)
outs = net.forward(output_layers)

# Showing informations on the screen
class_ids = []
confidences = []
boxes = []
for out in outs:
    for detection in out:
        scores = detection[5:]
        class_id = np.argmax(scores)
        confidence = scores[class_id]
        if confidence > 0.5:
            # Object detected
            center_x = int(detection[0] * width)
            center_y = int(detection[1] * height)
            w = int(detection[2] * width)
            h = int(detection[3] * height)

            # Rectangle coordinates
            x = int(center_x - w / 2)
            y = int(center_y - h / 2)

            boxes.append([x, y, w, h])
            confidences.append(float(confidence))
            class_ids.append(class_id)

indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)
print(indexes)
font = cv2.FONT_HERSHEY_PLAIN
for i in range(len(boxes)):
    if i in indexes:
        x, y, w, h = boxes[i]
        label = str(classes[class_ids[i]])
        color = colors[class_ids[i]]
        cv2.rectangle(img, (x, y), (x + w, y + h), color, 2)
        cv2.putText(img, label, (x, y + 30), font, 3, color, 3)

cv2.imshow("Image", img)
cv2.waitKey(0)
cv2.destroyAllWindows()
