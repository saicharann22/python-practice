
# Title
Facial Key points Detection and Extraction

# Description
The  program  is used for the detection
of the key features of the face in the given 
image like Eyes,Nose,Lips etc
Saving the respected features into
the respected folders


# Requirements 
- Open cv

- Os

- dlib(consists of ML algorithms)

- shape_predictor_68_face_landmarks.dat file

- Numpy


# Work flow 

- The input images are stored in "PICS" folder
  which we  needs the "OS module" to  access those
  images present in the folder one after the other

- The input image/file received from the pic folder 
  will be verified for file format and then the file
  or image is read and resized

- "Detector object" created using "get_frontal_face_detector" 
  to detect the faces in the image 

- "Grey scale image" of the input image using "CV2.COLOR_BGR2GRAY()" method
  is sent to the Detector object for the face detection

- For each face Detected in the image,(x,y) Co-Ordinates of the face are taken  
  and a bounding box is drawn accordingly 

- "Predictor object" obtained from shape_predictor_68_face_landmarks dat file
   using dlib module.
   Which predicts the shape of the face using 68 points
   as shown below

![App Screenshot](https://bitbucket.org/saicharann22/python-practice/raw/dd5b634aab56bf4ea8e4acc1213e102ac8d4d760/multiple_facial_key_points/facial_Detections/68_points.png)

-  The landmark points are stored in a list (my_points)
   Based on these landmark coordinates,the facial Key features are
   extracted by passing the landmark points to the defined function
   "feature_Box"

-  The user defined function(feature_Box) crops the image key features
   based on the landmark points and returns the cropped image

![App Screenshot](https://bitbucket.org/saicharann22/python-practice/raw/dd5b634aab56bf4ea8e4acc1213e102ac8d4d760/multiple_facial_key_points/output/single_face_feature_detection1.png)

- Using "imwrite" method the cropped images are stored into 
  into the respective folder

# Outputs
Detection of faces and key features
![App Screenshot](https://bitbucket.org/saicharann22/python-practice/raw/6f7d8b5f5da195aa7b61525d1bc58214a630fefc/multiple_facial_key_points/output/10.png)

![App Screenshot](https://bitbucket.org/saicharann22/python-practice/raw/6f7d8b5f5da195aa7b61525d1bc58214a630fefc/multiple_facial_key_points/output/11.png)

![App Screenshot](https://bitbucket.org/saicharann22/python-practice/raw/6f7d8b5f5da195aa7b61525d1bc58214a630fefc/multiple_facial_key_points/output/12.png)

![App Screenshot](https://bitbucket.org/saicharann22/python-practice/raw/6f7d8b5f5da195aa7b61525d1bc58214a630fefc/multiple_facial_key_points/output/13.png)



