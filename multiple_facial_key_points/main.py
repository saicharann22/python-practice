"""Detection of faces
   and extracting the key features
   and saving them into respective folders
"""
from cv2 import cv2
import dlib
import numpy as np
import os

number = 0

def feature_Box(img,points,scale=4):
    """Main function to extract the key features
       based on the given region obtained
       from 68_shape predictor dat file

       Crops the key feature region
       and return the respective feture
    """
    bbox = cv2.boundingRect(points)
    x,y,w,h = bbox
    imgcrop = img[y:y+h,x:x+w]
    imgcrop = cv2.resize(imgcrop,(0,0),None,scale,scale)
    return imgcrop  #return the cropped key feature
#m5 - 0.8
l = len(os.listdir("pics"))
print("length--",l)
i = 0

for file in os.listdir("pics"):
    i = i + 1
    if (file.endswith(".jpeg")  or file.endswith(".jpg")) and  i <= l:
        print(i)
        print(file)
        img = cv2.imread(file)
        img = cv2.resize(img,(0,0),None,0.22,0.22)
        img_original = img.copy()

        #object to find faces in the pics
        detector = dlib.get_frontal_face_detector()
        #object to detect the shape of the face
        predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

        imgGray  = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        faces =    detector(imgGray) #stores all faces in the image

        for face in faces:
            x1,y1 = face.left(),face.top()
            x2,y2 = face.right(),face.bottom()

            img_original = cv2.rectangle(img,(x1,y1),(x2,y2),(0,255,0),2) #face bound box
            landmarks = predictor(imgGray,face) #finds all landmarks in the face
            number = number + 1
            my_points = []
            for n in range(0,68):
                x = landmarks.part(n).x
                y = landmarks.part(n).y
                my_points.append([x,y])
                # (img,center_point,radius,color)--plotting the shape and features
                #cv2.circle(img_original,(x,y),2,(50,50,255),cv2.FILLED)--draws circle
                #cv2.putText(img_original,
                	      #str(n),(x,y-10),
                	      #cv2.FONT_HERSHEY_COMPLEX_SMALL,0.5,(0,0,255),1)

            my_points = np.array(my_points)

            lefteye =  feature_Box(img,my_points[36:42])
            righteye = feature_Box(img,my_points[42:48])
            nose = feature_Box(img,my_points[27:36])
            lips = feature_Box(img,my_points[48:61])

	    #shows the image and saves in the left_eye folder
            cv2.imshow("Lefteye",lefteye)
            cv2.imwrite('left_eye/left_eye' + str(number) +'.jpeg',lefteye)

	    #Display's the image and saves in the right_eye folder
            cv2.imshow("righteye",righteye)
            cv2.imwrite('right_eye/right_eye' + str(number) +'.jpeg',righteye)

	    #Display's the image and saves in the Nose folder
            cv2.imshow("Nose",nose)
            cv2.imwrite('Nose/nose' + str(number) +'.jpeg',nose)

	    #Display's the image and saves in the Lips folder
            cv2.imshow("Lips",lips)
            cv2.imwrite('Lips/lips' + str(number) +'.jpeg',lips)


            #print(my_points)
            cv2.imshow("Original",img_original) #shows images with bounding boxes
            cv2.waitKey(1000)
    else:
        break
